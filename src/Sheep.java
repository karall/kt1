import java.util.Arrays;


public class Sheep {

    enum Animal {sheep, goat}

    public static void main(String[] param) {

        Animal[] animalsArray = {Animal.sheep, Animal.goat, Animal.sheep, Animal.goat, Animal.goat, Animal.sheep, Animal.goat, Animal.goat, Animal.goat, Animal.sheep};
        System.out.println(Arrays.toString(animalsArray));
        reorder(animalsArray);
        System.out.println(Arrays.toString(animalsArray));

    }

    public static void reorder(Animal[] animals) {
        if (animals.length <= 1) {
            return;
        }

        int goatCount = countGoat(animals);

        for (int i = 0; i < goatCount; i++) {
            animals[i] = Animal.goat;
        }
        for (int i = goatCount; i < animals.length; i++) {
            animals[i] = Animal.sheep;
        }
    }

    private static int countGoat(Animal[] animals) {
        int count = 0;

        for (Animal animal : animals) {
            if (animal == Animal.goat) {
                count++;
            }
        }

        if (count <= animals.length) {
            return count;
        }

        return 0;
    }
}




